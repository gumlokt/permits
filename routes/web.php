<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();




// --- admin section ---
// ------------------------------------------------------------------------------------
Route::any('adminpanel', 'AdminController@index');


// --- adminpanel --- Emitters
Route::get('adminpanel/emitters', 'EmitterController@index');
Route::get('adminpanel/emitter/create', 'EmitterController@create');
Route::post('adminpanel/emitter/store', 'EmitterController@store');

Route::get('adminpanel/emitter/{id}/edit', 'EmitterController@edit');
Route::post('adminpanel/emitter/{id}/update', 'EmitterController@update');

Route::get('adminpanel/emitter/{id}/pause', 'EmitterController@pause');
Route::get('adminpanel/emitter/{id}/activate', 'EmitterController@activate');


// --- adminpanel --- Web-application settings
Route::get('adminpanel/settings/show', 'AdminController@show');
Route::post('adminpanel/settings/save', 'AdminController@save');
Route::post('adminpanel/settings/cancel', 'AdminController@cancel');




// --- admin section ---
// -----------------------------------------------------------------------------------------------------------//
// Emitters
Route::get('emitters/actual', 'AjaxController@actualEmitters');



// Logs
Route::post('logs/last', 'LogbookController@lastLogs');
Route::post('logs/plate', 'LogbookController@plateLogs');

Route::get('logs/summary', 'LogbookController@summary');

Route::post('logs/field/autocomplete', 'LogbookController@fieldAutocomplete');

Route::post('log/register/{direction}', 'LogbookController@register');
Route::get('log/{id}/reverse', 'LogbookController@reverse');



// Permits create/edit actions
Route::get('permits/plate/autocomplete', 'PermitController@plateAutocomplete');
Route::post('permits/field/autocomplete', 'PermitController@fieldAutocomplete');

Route::get('permits/summary', 'PermitController@summary');
Route::post('permits/getvalid', 'PermitController@validpermits');

Route::get('permit/number', 'PermitController@number');
Route::get('permit/last', 'PermitController@lastPermit');

Route::get('permit/{id}/edit', 'PermitController@edit');
Route::get('permit/{id}/invalidate', 'PermitController@invalidate');

Route::post('permit/store', 'PermitController@store');
Route::post('permit/update', 'PermitController@update');

Route::post('permit/transfer', 'PermitController@transfer');

Route::post('permits/print', 'PermitController@print');



// Filtering
Route::post('filtering/permits', 'AjaxController@filterPermits');
Route::post('filtering/search', 'AjaxController@filterSearch');



// Settings
Route::get('settings/permits', 'SettingController@permitsFilters');
Route::get('settings/search', 'SettingController@searchFilters');
Route::get('settings/setups', 'SettingController@getSetups');
Route::post('settings/setups', 'SettingController@saveSetups');



// Export to Excel
// Route::get('export', 'ExportController@index');



// All other routes
Route::get('{any}', function () {
    return view('index');
})->where('any', '.*');
