<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EmittersTableSeeder::class);

        // $virlual_logs = 25000; // number of rows to populate table logs with fake data
        // $permit_ids = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46 ];
        // $directions = [ 'in', 'out' ];
        // $customers = [ 'Пургаз', 'Пурнефтегаз', 'Пурнефть', 'Когалымнефтегаз' ];
        // $destinations = [ 'ГГП', 'Такро-Сале', 'Губкинский', 'ГГП, Северный купол', 'Санкт-Петербург', 'ДНС-2', 'ГГП (ОЗК)', 'Штаб', 'Ноябрьск', 'Москва', 'Сев. Губкинское м-е', 'Пурпе', 'Куст 32' ];

        // $time = time() - 525000; // 86400 sec at 1 day
        // $faker = Faker::create('ru_RU');

        // foreach (range(1, $virlual_logs) as $index) {
        //     $time += 21;
        //     $now = date('Y-m-d H:i:s', $time);

        //     DB::table('logs')->insert([
	    //         'permits_id' => $permit_ids[rand(0,23)],
	    //         'direction' => $directions[rand(0,1)],
	    //         'driver' => $faker->name,
	    //         'cargo' => $faker->realText(rand(10,25)),
	    //         'customer' => $customers[rand(0,3)],
	    //         'destination' => $destinations[rand(0,9)],
	    //         'comment' => $faker->realText(rand(10,20)),
	    //         'created_at' => $now,
	    //         'updated_at' => $now,
	    //     ]);
        // }


    }
}
