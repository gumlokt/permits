<?php

use Illuminate\Database\Seeder;

class EmittersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('emitters')->insert([
            [ 'abbr' => 'ПГ',  'emitter' => 'Пургаз', ],
        ]);
    }
}
