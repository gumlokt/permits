<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'name'          => 'Admin',
            'email' 		=> 'ivkakupshev@purgaz.com',
            'password' 		=> bcrypt('111111'),
            'created_at' 	=> strftime('%F %T'), // %F - same as Y-m-d 
            'updated_at' 	=> strftime('%F %T'), // %T - same as H:i:s 
        ]);
    }
}
