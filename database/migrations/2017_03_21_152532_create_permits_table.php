<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('permits', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('emitters_id')->unsigned()->index();
            $table->integer('owners_id')->unsigned()->index();
            $table->integer('cars_id')->unsigned()->index();

            $table->string('number')->index();
            $table->string('customer')->index();
            
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();

            
            // $table->string('transferred')->default(0);
            $table->boolean('transferred')->default(false);
            $table->string('person')->default('')->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('permits');
    }
}
