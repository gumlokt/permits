<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logbook extends Model {
    /**
     * By convention, the "snake case" plural name of the class 'Logbook' will be used as the table name (in this case 'logbooks').
     * But we specify a custom table by defining a '$table' property on the model. Now the table 'logs' (instead of 'logbooks') associated with the model 'Logbook'.
     *
     * @var string
     */
    protected $table = 'logs';

    protected $guarded = ['id', 'created_at', 'updated_at', ];

    public function permits() {
        return $this->belongsTo('App\Permit', 'permits_id');
    }


}
