<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

use App\Car;
use App\Permit;

use App\Logbook;


class LogbookController extends Controller {

    public function summary() {
        $summary['total'] = Logbook::count();
        $summary['in'] = Logbook::where('direction', '=', 'in' )->count();
        $summary['out'] = $summary['total'] - $summary['in'];

        return $summary;
    }



    public function fieldAutocomplete(Request $request) {
        $params = $request->input('params');

        $field = $params['field'];
        $value = $params['value'];

        $selection = Logbook::select($field)
            ->where($field, '!=', '')
            ->where(function ($query) use ($field, $value) {
                // $query->where($field, 'LIKE', '%' . $value . '%');
                $query->where($field, 'LIKE', $value . '%');
            })
            ->distinct()
            ->get();


        $result = [];
        foreach ($selection as $entry) {
            $result[] = $entry->$field;
        }


        return $result;
        return json_encode($result);
    }




    public function lastLogs(Request $request) {
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;

        // $yesterday = date('H:i:s');
        // dd($yesterday);
        // $yesterday = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 290);
        // $yesterday = date('Y-m-d H:i:s', time() - 60 * 60 * 24);
        $yesterday = '2018-10-23 00:00:01';

        // $logs = Logbook::where('created_at', '>', $yesterday)->orderBy('created_at', 'DESC')->get();
        // $logs = Logbook::take(30)->orderBy('created_at', 'DESC')->get();

        $totalRows = DB::table('logs')->count();

        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id',
                    'logs.direction',
                    'logs.driver',
                    'logs.cargo',
                    'logs.customer',
                    'logs.destination',
                    'logs.comment',
                    'logs.created_at',
                    'permits.number',
                    'emitters.emitter',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            // ->where('logs.created_at', '>', $yesterday)
            ->orderBy('logs.created_at', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        return [ 'totalRows' => $totalRows, 'logs' => $logs ];
        return $logs;
    }





    public function plateLogs(Request $request) {
        $plate = $request->input('plate');
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;

        $totalRows = DB::table('logs')
        ->join('permits', 'logs.permits_id', '=', 'permits.id')
        ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
        ->join('owners', 'permits.owners_id', '=', 'owners.id')
        ->join('cars', 'permits.cars_id', '=', 'cars.id')
        ->select(
                'logs.id',
                'logs.direction',
                'logs.driver',
                'logs.cargo',
                'logs.customer',
                'logs.destination',
                'logs.comment',
                'logs.created_at',
                'permits.number',
                'emitters.emitter',
                'owners.owner',
                'cars.plate',
                'cars.model'
            )
        ->where('cars.plate', '=', $plate)
        ->count();

        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id',
                    'logs.direction',
                    'logs.driver',
                    'logs.cargo',
                    'logs.customer',
                    'logs.destination',
                    'logs.comment',
                    'logs.created_at',
                    'permits.number',
                    'emitters.emitter',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            ->where('cars.plate', '=', $plate)
            ->orderBy('logs.created_at', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        // $totalRows = count($logs);
        // dd(gettype($logs));
        // $logs = array_slice($logs, ($page - 1) * $rows, $rows);;

        return [ 'totalRows' => $totalRows, 'logs' => $logs ];
    }






    // register event
    public function register(Request $request, $direction) {
        $this->validate($request, [
            'permitId'      => 'required',
            'driver'        => 'required',
            'cargo'         => 'required',
            'customer'      => 'required',
            'destination'   => 'required',
        ]);


        $sql = [
            'direction'     => $direction,
            'permits_id'    => $request->input('permitId'),
            'driver'        => $request->input('driver'),
            'cargo'         => $request->input('cargo'),
            'customer'      => $request->input('customer'),
            'destination'   => $request->input('destination'),
            'comment'       => $request->input('comment'),
        ];


        $log  = Logbook::create($sql);


        return 'All data was successfully saved...';
    }




    // inverse in/out direction
    public function reverse(Request $request, $id) {
        $log = Logbook::find($id);

        if ('in' == $log->direction) {
            $log->direction = 'out';
        } else {
            $log->direction = 'in';
        }

        if ($log->save()) {
            return ['error' => false, 'message' => 'Reversing was completed successfully...'];
        } else {
            return ['error' => true, 'message' => 'Reversing has not performed...'];
        }


    }




}
