<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

use App\Setting;

use App\Emitter;
use App\Owner;
use App\Car;

use App\Permit;
use App\Logbook;




class PermitController extends Controller {

    public function plateAutocomplete(Request $request) {
        $setting = Setting::where('parameter', '=', 'includeExpiredPermits')->first();

        $includeExpiredPermits = 1;
        if (!empty($setting) and '0' == $setting->value) {
            $includeExpiredPermits = 0;
        }

        $cars = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->select('emitters.emitter', 'emitters.abbr', 'owners.owner', 'cars.model', 'cars.plate', 'permits.id', 'permits.number', 'permits.start', 'permits.end')
            ->where('plate', 'LIKE', '%' . $request->input('phrase') . '%')
            ->when(!$includeExpiredPermits, function ($query) {
                return $query->where('end', '>=', date('Y-m-d') . ' 00:00:00');
            })
            ->orderBy('end', 'DESC')
            ->get();

        return $cars;
    }




    public function fieldAutocomplete(Request $request) {
        $params = $request->input('params');

        $field = $params['field'];
        $value = $params['value'];

        switch ($field) {
            case 'number': $table = 'permits'; break;
            case 'customer': $table = 'permits'; break;
            case 'person': $table = 'permits'; break;
            case 'owner': $table = 'owners'; break;
            default: $table = 'cars'; break;
        }

        $selection = DB::table($table)
            ->select($field)
            ->where($field, '!=', '')
            ->where(function ($query) use ($field, $value) {
                    $query->where($field, 'LIKE', '%' . $value . '%');
                })
            ->distinct()
            ->orderBy($field, 'ASC')
            ->get();

        $result = [];
        foreach ($selection as $entry) {
            $result[] = $entry->$field;
        }


        return $result;
    }




    public function summary() {
        $summary['total'] = Permit::count();
        $summary['valid'] = Permit::where('end', '>', date('Y-m-d H:i:s'))->count();
        $summary['expired'] = $summary['total'] - $summary['valid'];

        return $summary;
    }


    public function lastPermit() {
        $permit = Permit::orderBy('id', 'DESC')->first();

        return $permit;
    }




    public function number() {
        $permit = Permit::orderBy('id', 'DESC')->first();

        return $permit->number;
    }




    public function validpermits(Request $request) {
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;

        $setting = Setting::where('parameter', '=', 'permitsAtPage')->first();
        if (!empty($setting)) {
            $rows = $setting->value;
        }


        $now = date('Y-m-d H:i:s');

        $totalRows = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'permits.id',
                    'permits.number',
                    'permits.customer',
                    'permits.weapon',
                    'permits.privileged',
                    'permits.kpp1',
                    'permits.kpp2',
                    'permits.start',
                    'permits.end',
                    'permits.transferred',
                    'permits.person',
                    'emitters.emitter',
                    'emitters.abbr',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            ->where('permits.end', '>', $now)
            ->count();


            $permits = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'permits.id',
                    'permits.number',
                    'permits.customer',
                    'permits.weapon',
                    'permits.privileged',
                    'permits.kpp1',
                    'permits.kpp2',
                    'permits.start',
                    'permits.end',
                    'permits.transferred',
                    'permits.person',
                    'emitters.emitter',
                    'emitters.abbr',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            ->where('permits.end', '>', $now)
            ->orderBy('permits.created_at', 'DESC')
            ->orderBy('permits.number', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        return [ 'totalRows' => $totalRows, 'permits' => $permits ];
    }







    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $permit = DB::table('permits')
        ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
        ->join('owners', 'permits.owners_id', '=', 'owners.id')
        ->join('cars', 'permits.cars_id', '=', 'cars.id')
        ->select(
                'permits.id',
                'permits.number',
                'permits.customer',
                'permits.weapon',
                'permits.privileged',
                'permits.kpp1',
                'permits.kpp2',
                'permits.start',
                'permits.end',
                'permits.transferred',
                'permits.person',
                'emitters.emitter',
                'emitters.abbr',
                'owners.owner',
                'cars.plate',
                'cars.model'
            )
        ->where('permits.id', '=', $id)
        ->get();

        return $permit;
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $now = date('Y-m-d H:i:s');

        $this->validate($request, [
            'number'    => 'required|numeric',
            'model'     => 'required',
            'plate'     => 'required',

            'owner'     => 'required',
            'customer'  => 'required',
            'weapon'    => 'required',
            'privileged'=> 'required',

            'kpp1'      => 'required',
            'kpp2'      => 'required',

            'start'     => 'required|date_format:d.m.Y',
            'end'       => 'required|date_format:d.m.Y',
        ]);


        $emitter = Emitter::where('emitter', '=', $request->input('emitter'))->first();


        // start date can not be older end date
        if(date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s') > date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 00:00:00'), 'Y-m-d H:i:s')) {
            return ['error' => true, 'message' => 'Дата начала действия пропуска не может быть позднее даты окончания действия пропуска.'];
        }

        // expired permits are not allowed
        if(date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'Y-m-d H:i:s') < $now) {
            return ['error' => true, 'message' => 'Не допускается добавление пропуска с заведомо истёкшим сроком действия.'];
        }



        $model      = $this->clearSpaces($request->input('model'));
        $plate      = $this->clearPlate($request->input('plate'));

        $car        = Car::where('model', '=', $model)->where('plate', '=', $plate)->first();
        if (empty($car->id)) {
            $car    = Car::create(['model' => $model, 'plate' => $plate]);
        }
        $carId      = $car->id;



        // count valid permits
        $count = Permit::where('emitters_id', '=', $emitter->id)
            ->where('cars_id', '=', $carId)
            ->where('end', '>', $now)
            ->count();

        $emitter = Emitter::find($emitter->id);
        // do not allow register more than 2 valid permits
        if ($count > 1) {
            return ['error' => true, 'message' => 'В базе данных обнаружены 2 действующих пропуска выданных на автомобиль модели <strong>' . $request->input('model') . '</strong> с гос. номером <strong>' . $request->input('plate') . '</strong> эмитентом <strong>' . $emitter->emitter . '</strong>.<br><hr>Регистрация трёх и более пропусков на один и тот же автомобиль, принадлежащему одной и той же организации и выпущенных одним и тем же эмитентом <strong>запрещена</strong>.'];
        }


        // get latest permit (no matter - valid or not)
        $latestPermit = Permit::where('emitters_id', '=', $emitter->id)
            ->where('cars_id', '=', $carId)
            ->orderBy('end', 'DESC')
            ->first();

        if ($latestPermit and $latestPermit->end > date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s')) {
            return ['error' => true, 'message' => '<strong>Ошибка.</strong><br>Регистрация нового пропуска не произеведена, т. к. на этот же автомобиль уже зарегистрирован пропуск с датой выдачи <strong>' . date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'd.m.Y') . '</strong>. <br><hr><strong>Подробности.</strong><br>На автомобиль модели <strong>' . $latestPermit->cars->model . '</strong> с гос. номером <strong>' . $latestPermit->cars->plate . '</strong> уже зарегистрирован пропуск, который выдан организации <strong>' . $latestPermit->owners->owner . '</strong> эмитентом <strong>' . $latestPermit->emitters->emitter . '</strong> под номером <strong>&#8470; ' . $latestPermit->number . '</strong>, со сроком действия с <strong class="text-success">' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->start), 'd.m.Y') . '</strong> по <strong class="text-success">' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->end), 'd.m.Y') . '</strong>.<br><hr><strong>Способы решения.</strong><br><form action="' . url('permit/' . $latestPermit->id . '/edit') . '" method="GET" style="display: inline;"><button type="submit" class="btn btn-link" style="cursor: pointer; padding: 0;">Измените</button></form> другой <em>(действующий)</em> пропуск так, чтобы срок его действия заканчивался раньше даты начала действия вновь регистрируемого пропуска. Либо здесь <em>(в новом пропуске)</em> укажите дату выдачи после <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->end), 'd.m.Y') . '</strong>.'];
        }




        $company    = $this->clearSpaces($request->input('owner'));
        $owner      = Owner::where('owner', '=', $company)->first();
        if (empty($owner->owner)) {
            $owner  = Owner::create(['owner' => $company, 'actual' => 1]);
        }
        $ownerId    = $owner->id;

        $weapon     = 0;
        if ($request->input('weapon')) {
            $weapon = 1;
        }

        $privileged     = 0;
        if ($request->input('privileged')) {
            $privileged = 1;
        }

        $kpp1     = 0;
        if ($request->input('kpp1')) {
            $kpp1 = 1;
        }

        $kpp2     = 0;
        if ($request->input('kpp2')) {
            $kpp2 = 1;
        }

        $sql = [
            'emitters_id'   => $emitter->id,
            'owners_id'     => $ownerId,
            'cars_id'       => $carId,
            'number'        => $this->clearSpaces($request->input('number')),
            'customer'      => $this->clearSpaces($request->input('customer')),
            'weapon'        => $weapon,
            'privileged'    => $privileged,
            'kpp1'          => $kpp1,
            'kpp2'          => $kpp2,
            'start'         => date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s'),
            'end'           => date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'Y-m-d H:i:s'),
        ];

        $permit  = Permit::create($sql);


        return ['error' => false, 'message' => 'Пропуск успешно сохранен в базе данных...', 'permit' => $permit];
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $now = date('Y-m-d H:i:s');

        $this->validate($request, [
            'number'    => 'required',
            'customer'  => 'required',
            'weapon'    => 'required',
            'privileged'=> 'required',
            'kpp1'      => 'required',
            'kpp2'      => 'required',
            'owner'     => 'required',
            'start'     => 'required|date_format:d.m.Y',
            'model'     => 'required',
            'plate'     => 'required',
            'end'       => 'required|date_format:d.m.Y',
        ]);



        // start date can not be older end date
        if(date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s') > date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 00:00:00'), 'Y-m-d H:i:s')) {
            return ['error' => true, 'message' => 'Дата начала действия пропуска не может быть позднее даты окончания действия пропуска.'];
        }

        // expired permits are not allowed
        if(date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'Y-m-d H:i:s') < $now) {
            return ['error' => true, 'message' => 'Не допускается редактирование пропуска с истёкшим сроком действия.'];
        }


        // If user tries to set new start date of permit, we should check if that date is not later than first event date in log table
        $log = Logbook::where('permits_id', '=', $request->input('id'))->first();
        if(!empty($log) and date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s') > $log->created_at) {
            return ['error' => true, 'message' => 'Дата выдачи (начала действия) пропуска не может быть позднее даты первой записи в журнале событий (т. е., она не может быть позднее <strong>' . date_format($log->created_at, 'd.m.Y') . '</strong> ).'];
        }


        $permit  = Permit::find($request->input('id'));
        $existingCarId = $permit->cars_id;
        $existingOwnerId = $permit->owners_id;



        // Entered model and plate (the same, other existing or new)
        $model      = $this->clearSpaces($request->input('model'));
        $plate      = $this->clearPlate($request->input('plate'));
        $car        = Car::where('model', '=', $model)->where('plate', '=', $plate)->first();

        if (empty($car->id)) {
            // count other permits where existing car take place
            $otherPermits = Permit::where('id', '!=', $permit->id)->where('cars_id', '=', $existingCarId)->count();

            if ($otherPermits) {
                // create new car
                $car    = Car::create(['model' => $model, 'plate' => $plate]);
            } else {
                // update existing car
                $car = Car::find($existingCarId);
                $car->update(['model' => $model, 'plate' => $plate]);
            }
        } else {
            // update [overwrite] existing car - actually correct typos if they were
            $car->update(['model' => $model, 'plate' => $plate]);
        }
        $carId      = $car->id;


        $emitter = Emitter::where('emitter', '=', $request->input('emitter'))->first();

        // count valid permits
        $count = Permit::where('id', '!=', $request->input('id'))
            ->where('emitters_id', '=', $emitter->id)
            ->where('cars_id', '=', $carId)
            ->where('end', '>', $now)
            ->count();


        $emitter = Emitter::find($emitter->id);
        // do not allow register more than 2 valid permits
        if ($count > 1) {
            return ['error' => true, 'message' => 'В базе данных обнаружены 2 действующих пропуска выданных организации <strong>' . $request->input('owner') . '</strong> на автомобиль модели <strong>' . $request->input('model') . '</strong> с гос. номером <strong>' . $request->input('plate') . '</strong>, выпущенных эмитентом <strong>' . $emitter->emitter . '</strong>.<br><hr>Регистрация трёх и более пропусков на один и тот же автомобиль, выданных одной и той же организации и выпущенных одним и тем же эмитентом <strong>запрещена</strong>.'];
        }


        // get latest permit (no matter - valid or not)
        $latestPermit = Permit::where('id', '!=', $request->input('id'))
            ->where('emitters_id', '=', $emitter->id)
            ->where('cars_id', '=', $carId)
            ->orderBy('end', 'DESC')
            ->first();

        if ($latestPermit) {
            if ($latestPermit->start > $permit->end) {
                if (date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s') > $latestPermit->start) {
                    return ['error' => true, 'message' => 'Изменение пропуска не доступно, т. к., вы указали дату начала действия пропуска <strong>' . date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'd.m.Y') . '</strong>. Указанная дата входит в диапазон действия пропуска, выданного эмитентом <strong>' . $latestPermit->emitters->emitter . '</strong> под номером <strong>&#8470; ' . $latestPermit->number . '</strong> на автомобиль модели <strong>' . $latestPermit->cars->model . '</strong> с гос. номером <strong>' . $latestPermit->cars->plate . '</strong>, срок действия которого с <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->start), 'd.m.Y') . '</strong> по <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->end), 'd.m.Y') . '</strong>.<br><hr>Срок действия редактируемого пропуска не может начинаться позже даты начала действия другого зарегистрированного пропуска, срок дейсвия которого ещё не наступил.'];
                }

                if (date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'Y-m-d H:i:s') > $latestPermit->start) {
                    return ['error' => true, 'message' => 'Изменение пропуска не доступно, т. к., вы указали дату окончания действия пропуска <strong>' . date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'd.m.Y') . '</strong>. Указанная дата входит в диапазон действия пропуска, выданного эмитентом <strong>' . $latestPermit->emitters->emitter . '</strong> под номером <strong>&#8470; ' . $latestPermit->number . '</strong> на автомобиль модели <strong>' . $latestPermit->cars->model . '</strong> с гос. номером <strong>' . $latestPermit->cars->plate . '</strong>, срок действия которого с <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->start), 'd.m.Y') . '</strong> по <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->end), 'd.m.Y') . '</strong>.<br><hr>Срок действия редактируемого пропуска не может оканчиваться позже даты начала действия другого зарегистрированного пропуска, срок дейсвия которого ещё не наступил.'];
                }

                // get latest permit (no matter - valid or not)
                $expiredPermit = Permit::where('id', '!=', $request->input('id'))
                    ->where('id', '!=', $latestPermit->id)
                    ->where('emitters_id', '=', $emitter->id)
                    ->where('cars_id', '=', $carId)
                    ->orderBy('end', 'DESC')
                    ->first();

                if ($expiredPermit and $expiredPermit->end > date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s')) {
                    return ['error' => true, 'message' => 'Изменение пропуска не доступно, т. к., вы указали дату начала действия пропуска <strong>' . date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'd.m.Y') . '</strong>. Указанная дата входит в диапазон действия предыдущего и уже истёкшего пропуска, выданного эмитентом <strong>' . $expiredPermit->emitters->emitter . '</strong> под номером <strong>&#8470; ' . $expiredPermit->number . '</strong> на автомобиль модели <strong>' . $expiredPermit->cars->model . '</strong> с гос. номером <strong>' . $expiredPermit->cars->plate . '</strong>, срок действия которого был с <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $expiredPermit->start), 'd.m.Y') . '</strong> по <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $expiredPermit->end), 'd.m.Y') . '</strong>.<br><hr>Срок действия редактируемого пропуска не может начинаться раньше даты окончания действия предыдущего и уже истёкшего пропуска.'];
                }

            } else {
                if ($latestPermit->end > date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s')) {
                    return ['error' => true, 'message' => 'Изменение пропуска не доступно, т. к., вы указали дату начала действия пропуска <strong>' . date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'd.m.Y') . '</strong>. Указанная дата входит в диапазон действия предыдущего пропуска, выданного эмитентом <strong>' . $latestPermit->emitters->emitter . '</strong> под номером <strong>&#8470; ' . $latestPermit->number . '</strong> на автомобиль модели <strong>' . $latestPermit->cars->model . '</strong> с гос. номером <strong>' . $latestPermit->cars->plate . '</strong>, срок действия которого был с <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->start), 'd.m.Y') . '</strong> по <strong>' . date_format(date_create_from_format('Y-m-d H:i:s', $latestPermit->end), 'd.m.Y') . '</strong>.<br><hr>Срок действия редактируемого пропуска не может начинаться раньше даты окончания действия предыдущего пропуска.'];
                }
            }
        }



        // Entered owner from input field (it can be the same [existing] owner, other existing owner or new owner)
        $company    = $this->clearSpaces($request->input('owner'));
        $owner      = Owner::where('owner', '=', $company)->first();

        if (empty($owner->owner)) {
            // count other permits where existing owner take place
            $otherPermits = Permit::where('id', '!=', $permit->id)->where('owners_id', '=', $existingOwnerId)->count();

            if ($otherPermits) {
                // create new owner
                $owner  = Owner::create(['owner' => $company, 'actual' => 1]);
            } else {
                // update existing owner
                $owner = Owner::find($existingOwnerId);
                $owner->update(['owner' => $company]);
            }
        } else {
            // update [overwrite] existing owner - actually correct typos if they were
            $owner->update(['owner' => $company]);
        }
        $ownerId    = $owner->id;

        $weapon     = 0;
        if ($request->input('weapon')) {
            $weapon = 1;
        }

        $privileged     = 0;
        if ($request->input('privileged')) {
            $privileged = 1;
        }

        $kpp1     = 0;
        if ($request->input('kpp1')) {
            $kpp1 = 1;
        }

        $kpp2     = 0;
        if ($request->input('kpp2')) {
            $kpp2 = 1;
        }

        $sql = [
            'emitters_id'   => $emitter->id,
            'owners_id'     => $ownerId,
            'cars_id'       => $carId,
            'number'        => $request->input('number'),
            'customer'      => $request->input('customer'),
            'weapon'        => $weapon,
            'privileged'    => $privileged,
            'kpp1'          => $kpp1,
            'kpp2'          => $kpp2,
            'start'         => date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s'),
            'end'           => date_format(date_create_from_format('d.m.Y H:i:s', $request->input('end') . ' 23:59:59'), 'Y-m-d H:i:s'),
        ];


        $permit->update($sql);


        // Delete unused owners and cars from DB if they are exist
        $otherPermits = Permit::where('owners_id', '=', $existingOwnerId)->count();
        if (!$otherPermits) {
            Owner::destroy($existingOwnerId);
        }

        $otherPermits = Permit::where('cars_id', '=', $existingCarId)->count();
        if (!$otherPermits) {
            Car::destroy($existingCarId);
        }


        return ['error' => false, 'message' => 'Произведенные изменения успешно сохранены...'];
    }







    public function invalidate($id) {
        // return ['error' => false, 'message' => 'Пропуск успешно аннулирован...'];
        $permit  = Permit::find($id);
        $permit->end = date('Y-m-d H:i:s', time()-1); // сделать пропуск недействительным секундой назад
        // $permit->end = date_format(date_create_from_format('d.m.Y H:i:s', $request->input('start') . ' 00:00:00'), 'Y-m-d H:i:s'); // сделать пропуск недействительным секундой назад
        $permit->save();

        return ['error' => false, 'message' => 'Пропуск успешно аннулирован...'];
    }








    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }






    public function transfer(Request $request) {
        $this->validate($request, [ 'person'    => 'required' ]);

        $person = $this->clearSpaces($request->input('person'));
        $permits = $request->input('permits');

        foreach ($permits as $id) {
            $permit  = Permit::find($id);

            $permit->transferred = true;
            $permit->person = $person;

            $permit->save();
        }


        return ['error' => false, 'message' => 'Ответственный за прием пропусков успешно сохранен...'];
    }







    public function print(Request $request) {
        $ids = explode(",", $request->input('permits'));
        $permits = Permit::find($ids);

        $pages = [];
        $count = count($permits);
        for ($p = 0, $i = 0, $j = 0; $j < $count; $i++, $j++) {
            $pages[$p][$i] = $permits[$j];

            if (3 == $i) {
                $p++;
                $i = -1;
            }
        }

        $settings = [];
        $settings['position'] = Setting::where('parameter', '=', 'position')->first();
        $settings['name'] = Setting::where('parameter', '=', 'name')->first();


        return view('print', [ 'pages' => $pages, 'settings' => $settings ]);
    }











    private function clearSpaces($string) {
        $result = preg_replace('/\s{2,}/', ' ', $string);

        return $result;
    }

    private function clearPlate($plate) {
        // $plate = preg_replace('/\s+/', '', mb_strtoupper($plate)); // delete all whitespaces
        // $plate = preg_replace('/\s{2,}/', '', mb_strtoupper(trim($plate))); // delete all double whitespaces
        $plate = mb_strtoupper(trim($plate));

        return $plate;
    }


}
