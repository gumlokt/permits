<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Emitter;

class EmitterController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $emitters = Emitter::orderBy('actual', 'DESC')->get();


        return view('adminpanel.emitters.index', ['emitters' => $emitters]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('adminpanel.emitters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'abbr'      => 'required|unique:emitters,abbr',
            'emitter'   => 'required|unique:emitters,emitter',
        ]);

        Emitter::create(['abbr' => $request->input('abbr'), 'emitter' => $request->input('emitter')]);

        return redirect('adminpanel/emitters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('adminpanel/emitters');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $emitter = Emitter::find($id);

        return view('adminpanel.emitters.create', ['emitter' => $emitter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'abbr'      => 'required',
            'emitter'   => 'required',
        ]);

        $abbr = $request->input('abbr');
        $emitter = $request->input('emitter');

        $existAbbr = Emitter::where('id', '!=', $id)
            ->where('abbr', '=', $abbr)
            ->count();
        if ($existAbbr) {
            return back()->with('message', 'Эмитент с кратким наименованием <strong>' . $abbr . '</strong> уже зарегистрирован. Выберите другое краткое наименование.');
        }

        $existEmitter = Emitter::where('id', '!=', $id)
            ->where('emitter', '=', $emitter)
            ->count();
        if ($existEmitter) {
            return back()->with('message', 'Эмитент с полным наименованием <strong>' . $emitter . '</strong> уже зарегистрирован. Выберите другое полное наименование.');
        }


        Emitter::where('id', '=', $id)->update(['abbr' => $abbr, 'emitter' => $emitter]);

        return redirect('adminpanel/emitters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        return redirect('adminpanel/emitters');
    }













    public function pause($id) {
        Emitter::where('id', '=', $id)->update(['actual' => 0]);

        return redirect('adminpanel/emitters');
    }


    public function activate($id) {
        Emitter::where('id', '=', $id)->update(['actual' => 1]);

        return redirect('adminpanel/emitters');
    }









}
