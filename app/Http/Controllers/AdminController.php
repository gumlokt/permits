<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Emitter;
use App\Owner;
use App\Car;
use App\Permit;
use App\Logbook;


class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $stats['cars'] = Car::count();
        $stats['owners'] = Owner::count();

        $stats['totalLogs'] = Logbook::count();
        $stats['inLogs'] = Logbook::where('direction', '=', 'in')->count();

        $stats['firstLog'] = Logbook::first();
        $stats['lastLog'] = Logbook::orderBy('id', 'DESC')->first();

        $stats['totalPermits'] = Permit::count();
        $stats['validPermits'] = Permit::where('end', '>', date('Y-m-d H:i:s'))->count();

        $stats['totalEmitters'] = Emitter::count();
        $stats['validEmitters'] = Emitter::where('actual', '=', 1)->count();


        return view('adminpanel.index', ['stats' => $stats]);
    }


    // show settings page
    public function show() {
        return view('adminpanel.settings.index');
    }





}
