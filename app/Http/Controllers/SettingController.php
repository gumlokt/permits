<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;


class SettingController extends Controller {
    public function permitsFilters() {
        $setups = Setting::where('profile', '=', 'permits')->get();

        $settings = [];
        if(!empty($setups)) {
            foreach ($setups as $setup) {
                $settings[$setup->parameter] = $setup->value;

                if (!empty($setup->value)) {
                    if ('categories' == $setup->parameter or 'transfers' == $setup->parameter or 'emitters' == $setup->parameter) {
                        $settings[$setup->parameter] = unserialize($setup->value);
                    }
                }
            }
        } else {
            $settings = ['error' => true, 'message' => 'База данных не содержит настроек по фильтрации пропусков...'];
        }


        return $settings;
    }



    public function searchFilters() {
        $setups = Setting::where('profile', '=', 'search')->get();

        $settings = [];
        if(!empty($setups)) {
            foreach ($setups as $setup) {
                $settings[$setup->parameter] = $setup->value;

                if (!empty($setup->value)) {
                    if ('direction' == $setup->parameter or 'emitters' == $setup->parameter) {
                        $settings[$setup->parameter] = unserialize($setup->value);
                    }
                }
            }
        } else {
            $settings = ['error' => true, 'message' => 'База данных не содержит настроек по фильтрации въезда/выезда авто...'];
        }


        return $settings;
    }


    public function getSetups() {
        $setups = Setting::where('profile', '=', 'setups')->get();

        $settings = [];
        if(!empty($setups)) {
            foreach ($setups as $setup) {
                $settings[$setup->parameter] = $setup->value;
            }
        } else {
            $settings = ['error' => true, 'message' => 'База данных не содержит общих настроек приложения...'];
        }


        return $settings;
    }


    public function saveSetups(Request $request) {
        $newSettings = $request->input('settings');

        $settings = [];
        foreach ($newSettings as $parameter => $value) {
            $setup = Setting::where('profile', '=', 'setups')->where('parameter', '=', $parameter)->first();

            if(!empty($setup)) {
                $setup->update([ 'value' => $value ? $value : '' ]);
            } else {
                $setup = Setting::create([ 'profile' => 'setups', 'parameter' => $parameter, 'value' => $value ]);
            }

            $settings[$parameter] = $value;
        }


        return $settings = ['error' => false, 'message' => 'Настройки сохранены успешно...'];;
    }


}
