<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Log;

use DB;

use App\Setting;

use App\Emitter;
use App\Owner;
use App\Car;

use App\Permit;
use App\Logbook;


class AjaxController extends Controller {

    public function actualEmitters(Request $request) {
        $emitters = Emitter::where('actual', '=', 1)->orderBy('actual', 'emitter')->get();
        // $emitters = Emitter::orderBy('actual', 'emitter')->get();

        return json_encode($emitters);
    }





    public function filterPermits(Request $request) {
        $filters = $request->input('filters');
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;


        $categories     = $filters['categories'];

        $transfers      = $filters['transfers'];
        // $transfer       = isset($filters['transfers'][0]) ? $filters['transfers'][0] : 'no';

        $emitters       = $filters['emitters'];

        $plate          = $filters['plate'];
        $model          = $filters['model'];
        $owner          = $filters['owner'];

        $customer       = $filters['customer'];
        $weapon         = $filters['weapon'];
        $privileged     = $filters['privileged'];
        $person         = $filters['person'];

        $kpp1           = $filters['kpp1'];
        $kpp2           = $filters['kpp2'];

        $number         = $filters['number'];
        $start          = '';
        $end            = '';
        // Log::info('Value in the weapon variale: ' . $weapon);
        // Log::info('Value in the weapon variale: ' . $request->input('filters')['weapon']);

        if (!empty($filters['start'])) {
            $start =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['start'] . ' 00:00:00'), 'Y-m-d H:i:s');
        }
        if (!empty($filters['end'])) {
            $end =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['end'] . ' 23:59:59'), 'Y-m-d H:i:s');
        }


        // Firstly, save selected filtering settings to DB
        $setups = [
            'categories'   => count($categories) > 0 ? serialize($categories) : '',
            'transfers'    => count($filters['transfers']) > 0 ? serialize($filters['transfers']) : '',
            'emitters'     => count($emitters) > 0 ? serialize($emitters) : '',
            'plate'        => !empty($plate) ? $plate : '',
            'model'        => !empty($model) ? $model : '',
            'owner'        => !empty($owner) ? $owner : '',
            'number'       => !empty($number) ? $number : '',
            'customer'     => !empty($customer) ? $customer : '',
            'weapon'       => !empty($weapon) ? $weapon : '',
            'privileged'   => !empty($privileged) ? $privileged : '',
            'kpp1'         => !empty($kpp1) ? $kpp1 : '',
            'kpp2'         => !empty($kpp2) ? $kpp2 : '',
            'person'       => !empty($person) ? $person : '',
            'start'        => $start,
            'end'          => $end,
         ];

        foreach ($setups as $parameter => $value) {
            $setup = Setting::where('profile', '=', 'permits')->where('parameter', '=', $parameter)->first();

            if(!empty($setup)) {
                $setup->update([ 'value' => $value ]);
            } else {
                $setup = Setting::create([ 'profile' => 'permits', 'parameter' => $parameter, 'value' => $value ]);
            }
        }


        // Secondly, search and return requested permits
        $totalRows = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->select('emitters.emitter', 'emitters.abbr', 'owners.owner', 'cars.model', 'cars.plate', 'permits.id', 'permits.number', 'permits.customer', 'permits.weapon', 'permits.privileged', 'permits.kpp1', 'permits.kpp2', 'permits.start', 'permits.end', 'permits.transferred', 'permits.person')
            ->when($categories, function ($query, $categories) {
                if (count($categories) == 1) {
                    $now = date('Y-m-d H:i:s');

                    if ('valid' == $categories[0]) {
                        return $query->where('end', '>=', $now);
                    } else {
                        return $query->where('end', '<=', $now);
                    }
                }
            })
            ->when($transfers, function ($query, $transfers) {
                if (1 == count($transfers)) {
                    if ('yes' == $transfers[0]) {
                        return $query->where('transferred', '=', 1);
                    } else {
                        return $query->where('transferred', '=', 0);
                    }
                }
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($number, function ($query, $number) {
                return $query->where('number', 'LIKE', '%' . $number . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($weapon, function ($query, $weapon) {
                return $query->where('weapon', 'LIKE', '%' . $weapon . '%');
            })
            ->when($privileged, function ($query, $privileged) {
                return $query->where('privileged', 'LIKE', '%' . $privileged . '%');
            })
            ->when($kpp1, function ($query, $kpp1) {
                return $query->where('kpp1', 'LIKE', '%' . $kpp1 . '%');
            })
            ->when($kpp2, function ($query, $kpp2) {
                return $query->where('kpp2', 'LIKE', '%' . $kpp2 . '%');
            })
            ->when($person, function ($query, $person) {
                return $query->where('person', 'LIKE', '%' . $person . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('start', '=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('end', '=', $end);
            })
            ->count();


        $permits = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->select('emitters.emitter', 'emitters.abbr', 'owners.owner', 'cars.model', 'cars.plate', 'permits.id', 'permits.number', 'permits.customer', 'permits.weapon', 'permits.privileged', 'permits.kpp1', 'permits.kpp2', 'permits.start', 'permits.end', 'permits.transferred', 'permits.person')
            ->when($categories, function ($query, $categories) {
                if (count($categories) == 1) {
                    $now = date('Y-m-d H:i:s');

                    if ('valid' == $categories[0]) {
                        return $query->where('end', '>=', $now);
                    } else {
                        return $query->where('end', '<=', $now);
                    }
                }
            })
            ->when($transfers, function ($query, $transfers) {
                if (1 == count($transfers)) {
                    if ('yes' == $transfers[0]) {
                        return $query->where('transferred', '=', 1);
                    } else {
                        return $query->where('transferred', '=', 0);
                    }
                }
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($number, function ($query, $number) {
                return $query->where('number', 'LIKE', '%' . $number . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($weapon, function ($query, $weapon) {
                return $query->where('weapon', 'LIKE', '%' . $weapon . '%');
            })
            ->when($privileged, function ($query, $privileged) {
                return $query->where('privileged', 'LIKE', '%' . $privileged . '%');
            })
            ->when($kpp1, function ($query, $kpp1) {
                return $query->where('kpp1', 'LIKE', '%' . $kpp1 . '%');
            })
            ->when($kpp2, function ($query, $kpp2) {
                return $query->where('kpp2', 'LIKE', '%' . $kpp2 . '%');
            })
            ->when($person, function ($query, $person) {
                return $query->where('person', 'LIKE', '%' . $person . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('start', '=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('end', '=', $end);
            })
            ->orderBy('permits.created_at', 'DESC')
            ->orderBy('permits.number', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        return [ 'totalRows' => $totalRows, 'permits' => $permits ];
    }

















    public function filterSearch(Request $request) {
        $filters = $request->input('plate');
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;


        $direction      = $filters['direction'];
        $emitters       = $filters['emitters'];
        $plate          = $filters['plate'];
        $model          = $filters['model'];
        $owner          = $filters['owner'];

        $customer       = $filters['customer'];
        $driver         = $filters['driver'];
        $destination    = $filters['destination'];
        $cargo          = $filters['cargo'];

        $start = '';
        $end = '';

        if (!empty($filters['start'])) {
            $start =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['start'] . ' 00:00:00'), 'Y-m-d H:i:s');
        }
        if (!empty($filters['end'])) {
            $end =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['end'] . ' 23:59:59'), 'Y-m-d H:i:s');
        }


        // Firstly, save selected filtering settings to DB
        $setups = [
            'direction'    => count($direction) > 0 ? serialize($direction) : '',
            'emitters'     => count($emitters) > 0 ? serialize($emitters) : '',
            'plate'        => !empty($plate) ? $plate : '',
            'model'        => !empty($model) ? $model : '',
            'owner'        => !empty($owner) ? $owner : '',
            'customer'     => !empty($customer) ? $customer : '',
            'driver'       => !empty($driver) ? $driver : '',
            'destination'  => !empty($destination) ? $destination : '',
            'cargo'        => !empty($cargo) ? $cargo : '',
            'start'        => $start,
            'end'          => $end,
        ];

        foreach ($setups as $parameter => $value) {
            $setup = Setting::where('profile', '=', 'search')->where('parameter', '=', $parameter)->first();

            if(!empty($setup)) {
                $setup->update([ 'value' => $value ]);
            } else {
                $setup = Setting::create([ 'profile' => 'search', 'parameter' => $parameter, 'value' => $value ]);
            }
        }





        // Secondly, search and return requested data
        $totalRows = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id',
                    'logs.direction',
                    'logs.driver',
                    'logs.cargo',
                    'logs.customer',
                    'logs.destination',
                    'logs.comment',
                    'logs.created_at',
                    'permits.number',
                    'emitters.emitter',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            ->when($direction, function ($query, $direction) {
                if (count($direction) == 1) {
                    return $query->where('direction', '=', $direction[0]);
                }
            })
            ->when($driver, function ($query, $driver) {
                return $query->where('driver', 'LIKE', '%' . $driver . '%');
            })
            ->when($cargo, function ($query, $cargo) {
                return $query->where('cargo', 'LIKE', '%' . $cargo . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($destination, function ($query, $destination) {
                return $query->where('destination', 'LIKE', '%' . $destination . '%');
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('logs.created_at', '>=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('logs.created_at', '<=', $end);
            })
            ->count();

        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id',
                    'logs.direction',
                    'logs.driver',
                    'logs.cargo',
                    'logs.customer',
                    'logs.destination',
                    'logs.comment',
                    'logs.created_at',
                    'permits.number',
                    'emitters.emitter',
                    'owners.owner',
                    'cars.plate',
                    'cars.model'
                )
            ->when($direction, function ($query, $direction) {
                if (count($direction) == 1) {
                    return $query->where('direction', '=', $direction[0]);
                }
            })
            ->when($driver, function ($query, $driver) {
                return $query->where('driver', 'LIKE', '%' . $driver . '%');
            })
            ->when($cargo, function ($query, $cargo) {
                return $query->where('cargo', 'LIKE', '%' . $cargo . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($destination, function ($query, $destination) {
                return $query->where('destination', 'LIKE', '%' . $destination . '%');
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('logs.created_at', '>=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('logs.created_at', '<=', $end);
            })
            ->orderBy('logs.created_at', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        return [ 'totalRows' => $totalRows, 'logs' => $logs ];
    }



}
