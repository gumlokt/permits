<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>КПП ЗАО Пургаз</title>

    <link href="{{ asset('css/print.css') }}" rel="stylesheet" type="text/css">
</head>

<body>

    @foreach ($pages as $page)
    <page>
        @foreach ($page as $permit)
        <div class="box-outer">
            <div class="box-middle">
                <div class="box-inner">


                    <table class="decorative">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                    @if ($permit->weapon)
                    <img src="/images/gun.svg" class="weapon" alt="Оружие">
                    @endif

                    <table class="work">
                        <tr>
                            <td class="logo">
                                <img src="/images/logo.png" alt="logo">
                            </td>

                            <td class="zao">
                                <div>
                                    АКЦИОНЕРНОЕ ОБЩЕСТВО
                                </div>
                            </td>

                            <td class="content">
                                <div class="header">
                                    <div class="permit">ПРОПУСК №</div>
                                    <div class="number">{{ $permit->number }}</div>
                                </div>

                                <div class="place">
                                    на территорию Губкинского нефтегазового месторождения
                                </div>

                                <div class="modelPlate">
                                    <div class="model">
                                        Марка &mdash; {{ $permit->cars->model }}
                                    </div>
                                    <div class="plate">
                                        Гос.ном. &mdash; <span>{{ $permit->cars->plate }}</span>
                                    </div>
                                </div>

                                <div class="owner">
                                    Принадлежность &mdash; <span>{{ $permit->owners->owner }}</span>
                                </div>

                                <div class="customer">
                                    Заказчик &mdash; <span>{{ $permit->customer }}</span>
                                </div>

                                <div class="start">
                                    Срок дейсвия: <span>{{ date_format(date_create($permit->start), 'd.m.Y') }} г. &#8210; {{ date_format(date_create($permit->end), 'd.m.Y') }} г.</span>
                                </div>

                                <div class="worker">
                                    <div class="position">
                                        @if (isset($settings['position']) and !empty($settings['position']->value))
                                        {{ $settings['position']->value }}
                                        @else
                                        <span style="font-size: normal; font-weight: 300; color: red; border: 1px solid red; padding: 2px 6px;">Внимание! Введите наименование должности</span>
                                        @endif
                                    </div>
                                    <div class="name">
                                        @if (isset($settings['name']) and !empty($settings['name']->value))
                                        {{ $settings['name']->value }}
                                        @else
                                        <span style="font-size: normal; font-weight: 300; color: red; border: 1px solid red; padding: 2px 6px;">Введите ФИО</span>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="start">
                                                <span>Выдан: {{ date_format(date_create($permit->start), 'd.m.Y') }} г.</span>
                                            </div> -->

                                <!-- <div class="end">
                                                <div class="expires">
                                                    Действителен по: <span>{{ date_format(date_create($permit->end), 'd.m.Y') }} г.</span>
                                                </div>
                                            </div> -->


                                <div class="end">
                                    @if ($permit->kpp1)
                                    <div class="allowed">
                                        <img class="kpp__check" src="/images/check.svg" alt="check icon">
                                        <h2 class="kpp__title">КПП-1</h2>
                                    </div>
                                    @else
                                    <div class="prohibited">
                                        <img class="kpp__clear" src="/images/clear.svg" alt="clear icon">
                                        <h2 class="kpp__title">КПП-1</h2>
                                    </div>
                                    @endif

                                    @if ($permit->kpp2)
                                    <div class="allowed">
                                        <img class="kpp__check" src="/images/check.svg" alt="check icon">
                                        <h2 class="kpp__title">КПП-2</h2>
                                    </div>
                                    @else
                                    <div class="prohibited">
                                        <img class="kpp__clear" src="/images/clear.svg" alt="clear icon">
                                        <h2 class="kpp__title">КПП-2</h2>
                                    </div>
                                    @endif

                                    @if ($permit->privileged)
                                    <img src="/images/privileged.svg" class="privileged" alt="Без досмотра">
                                    @endif

                                </div>


                            </td>
                        </tr>
                    </table>


                </div>
            </div>
        </div>
        @endforeach
    </page>
    @endforeach


</body>

</html>