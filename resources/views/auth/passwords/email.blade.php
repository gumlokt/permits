@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-6">

        <div class="card">
            <div class="card-header">
                <h5 class="text-center text-danger">Восстановление пароля</h5>
            </div>

            <div class="card-block">
                <form action="{{ route('password.email') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group row{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email" class="col-sm-4 col-form-label"><i class="fa fa-envelope-o" aria-hidden="true"></i> E-Mail</label>
                        <div class="col-sm-8">
                            <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-8">
                            <button type="submit" class="btn btn-secondary"><i class="fa fa-check" aria-hidden="true"></i> Начать восстановление</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        @if (session('status'))
            <br>
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
</div>
@endsection
