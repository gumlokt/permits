@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-6">

        <div class="card">
            <div class="card-header">
                <h5 class="text-center text-danger">Вход в панель управления</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group row{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email" class="col-sm-4 col-form-label"><i class="fa fa-envelope"></i> E-Mail</label>
                        <div class="col-sm-8">
                            <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('password') ? ' has-danger' : '' }}">
                        <label for="password" class="col-sm-4 col-form-label"><i class="fa fa-key"></i> Пароль</label>
                        <div class="col-sm-8">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Пароль" required>

                            @if ($errors->has('password'))
                                <div class="form-control-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-8">
                            <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-sign-in-alt"></i> Войти</button>
                            <a class="btn btn-link" href="{{ route('password.request') }}">Забыли пароль?</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>




        <div class="row">
            <div class="col text-center">
                <div class="dropdown-divider"></div>
                <small class="text-muted">Built on Laravel {{ App::version() }}</small>
            </div>
        </div>

    </div>
</div>
@endsection
