@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <h5 class="card-header">
                    <div class="row">
                        <div class="col text-danger">
                            {{ isset($emitter) ? 'Редактировать' : 'Добавить нового' }} эмитента пропусков
                        </div>
                        <div class="col text-right text-danger">
                            <form style="display: inline; margin: 0; padding: 0;">
                                <button class="btn btn-outline-secondary btn-sm tooltips" type="submit" formaction="{{ url()->previous() }}" class="tooltips" data-toggle="tooltip" data-placement="top" data-html="true" title="Вернуться назад"><i class="fas fa-long-arrow-alt-left"></i> Отменить</button>
                            </form>
                        </div>
                    </div>
                </h5>

                <div class="card-body">


                    @if (isset($emitter))
                        <form action="{{ url('adminpanel/emitter/' . $emitter->id . '/update') }}" method="POST">
                    @else
                        <form action="{{ url('adminpanel/emitter/store') }}" method="POST">
                    @endif
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-8">

                            <div class="form-group row">
                                <label for="abbr" class="col-sm-4 col-form-label">Краткое наименование</label>
                                <div class="col-sm-8">
                                    <input type="text" name="abbr" class="form-control" placeholder="Например, ПГ" value="{{ isset($emitter) ? $emitter->abbr : old('abbr') }}" id="abbr">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="emitter" class="col-sm-4 col-form-label">Полное наименование</label>
                                <div class="col-sm-8">
                                    <input type="text" name="emitter" class="form-control" placeholder='Например, Пургаз' value="{{ isset($emitter) ? $emitter->emitter : old('emitter') }}" id="emitter">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-sm-4 col-sm-8">
                                    @if (isset($emitter))
                                        <button type="submit" class="btn btn-outline-success"><i class="fa fa-save"></i> Сохранить</button>
                                    @else
                                        <button type="submit" class="btn btn-outline-success"><i class="fa fa-plus"></i> Создать</button>
                                    @endif
                                </div>
                            </div>

                            </div>
                        </div>
                    </form>


                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @foreach ($errors->all() as $error)
                                {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif

                    @if (session('message'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            {!! session('message') !!}
                        </div>
                    @endif

                </div>
            </div>


        </div>
    </div>







@endsection
