@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">


                <h5 class="card-header">
                    <div class="row">
                        <div class="col text-danger">
                            Эмитенты пропусков
                        </div>
                        <div class="col text-right text-danger">
                            <form style="display: inline; margin: 0; padding: 0;">
                                <button class="btn btn-outline-primary btn-sm tooltips" type="submit" formaction="{{ url('adminpanel/emitter/create') }}" class="tooltips" data-toggle="tooltip" data-placement="top" data-html="true" title="Добавить нового эмитента пропусков">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </h5>

                <div class="card-body">
                    <table class="table table-hover table-bordered table-sm">
                        <thead>
                            <tr class="table-info">
                                <th>Краткое наименование</th>
                                <th>Полное наименование</th>
                                <th>Актуальность</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($emitters as $emitter)
                                <tr class="emitter">
                                    <td>{{ $emitter->abbr }}</td>
                                    <td>{{ $emitter->emitter }}</td>
                                    <td>
                                        @if($emitter->actual)
                                            <span class="text-success">Актуальный</span>
                                        @else
                                            <span class="text-muted">Не актуальный</span>
                                        @endif

                                        <form class="form-inline">
                                            @if($emitter->actual)
                                                <button class="btn btn-outline-success btn-sm tooltips" type="submit" formaction="{{ url('/adminpanel/emitter/' . $emitter->id . '/pause') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Изменить статус на <strong class='text-danger'>Не актуальный</strong>"><i class="fa fa-pause"></i></button>
                                            @else
                                                <button class="btn btn-outline-secondary btn-sm tooltips" type="submit" formaction="{{ url('/adminpanel/emitter/' . $emitter->id . '/activate') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Изменить статус на <strong class='text-success'>Актуальный</strong>"><i class="fa fa-play"></i></button>
                                            @endif

                                            <button class="btn btn-outline-warning btn-sm tooltips" type="submit" formaction="{{ url('/adminpanel/emitter/' . $emitter->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Редактировать эмитента"><i class="fa fa-pencil-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <strong class="text-danger">*</strong> <em>Статус эмитента пропусков <strong>Не актуальный</strong> означает то, что в форме добавления новых пропусков не актуальный эмитент будет отсутствовать в списке выбора.</em>
                </div>


            </div>
        </div>
    </div>
@endsection
