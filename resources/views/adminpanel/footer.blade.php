    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- <span class="text-muted">&copy; 2018-{{ date('Y') }} <span class="text-danger">КПП ЗАО «Пургаз»</span></span> -->
                    <span class="text-muted">&copy; {{ '2018' == date('Y') ? date('Y') : '2018-' . date('Y') }} <span class="text-danger">КПП ЗАО «Пургаз»</span></span>
                </div>

                <div class="col text-center">
                </div>

                <div class="col">
                    <div class="text-right">
                        @if (!Auth::guest())
                            Вы вошли как: <strong class="text-danger">{{ Auth::user()->name }}</strong> |
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out-alt text-danger"></i> Выход</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="{{ route('login') }}"><i class="fa fa-user-secret"></i> Вход</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </footer>
