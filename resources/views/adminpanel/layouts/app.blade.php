<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('ico/favicon.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Пургаз</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/fontawesome-free-5.5.0-web/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <script>
        window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token(), ]) !!};
    </script>
</head>
<body>

    @include('adminpanel.navbar')
    <div class="container" id="adminPanel">
        @yield('content')
    </div>

    @include('adminpanel.footer')
</body>
</html>
