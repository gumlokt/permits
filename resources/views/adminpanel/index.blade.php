@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">


            <div class="card">
                <h5 class="card-header text-danger">
                    Сводка
                </h5>

                <div class="card-body">


                    <div class="card-deck">
                        <div class="card bg-light text-center">
                            <div class="card-header">Текущие дата и время</div>
                            <div class="card-body"><span class="text-danger" id="timeBlock">@{{ currentTime }}</span></div>
                        </div>
                    </div>
                    <br>

                    <div class="card-deck">
                        <div class="card bg-light text-center">
                            <div class="card-header">Автомобилей</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ number_format($stats['cars'], 0, ',', ' ') }}</h5></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card bg-light text-center">
                            <div class="card-header">Владельцев авто</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ number_format($stats['owners'], 0, ',', ' ') }}</h5></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card bg-light text-center">
                            <div class="card-header">Записей журнала</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ number_format($stats['totalLogs'], 0, ',', ' ') }}</h5></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Въезд</em></td>
                                            <td class="text-right"><em class="text-success">{{ number_format($stats['inLogs'], 0, ',', ' ') }}</em></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Выезд</em></td>
                                            <td class="text-right"><em class="text-danger">{{ number_format(($stats['totalLogs'] - $stats['inLogs']), 0, ',', ' ') }}</em></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="card-deck">
                        <div class="card bg-light text-center">
                            <div class="card-header">Пропусков</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ $stats['totalPermits'] }}</h5></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Действующих</em></td>
                                            <td class="text-right"><em class="text-success">{{ $stats['validPermits'] }}</em></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Истекших</em></td>
                                            <td class="text-right"><em class="text-danger">{{ $stats['totalPermits'] - $stats['validPermits'] }}</em></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card bg-light text-center">
                            <div class="card-header">Эмитентов</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ $stats['totalEmitters'] }}</h5></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Актуальных</em></td>
                                            <td class="text-right"><em class="text-success">{{ $stats['validEmitters'] }}</em></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Не актуальных</em></td>
                                            <td class="text-right"><em class="text-danger">{{ $stats['totalEmitters'] - $stats['validEmitters'] }}</em></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card bg-light text-center">
                            <div class="card-header">Записей журнала</div>
                            <div class="card-body">
                                <table class="table table-sm table-hover table-borderless">
                                    <tbody class="text-muted">
                                        <tr>
                                            <td class="text-left"><h5 class="card-title">Всего</h5></td>
                                            <td class="text-right"><h5 class="card-title">{{ number_format($stats['totalLogs'], 0, ',', ' ') }}</h5></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Первая</em></td>
                                            <td class="text-right">
                                                <em class="text-success">
                                                    {{ date_format($stats['firstLog']->created_at, 'd.m.Y') }}
                                                    <code class="text-secondary" style="margin-left: 0.3em;">
                                                        {{ date_format($stats['firstLog']->created_at, 'H:i:s') }}
                                                    </code>
                                                </em>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><em>Последняя</em></td>
                                            <td class="text-right">
                                                <em class="text-danger">
                                                    {{ date_format($stats['lastLog']->created_at, 'd.m.Y') }}
                                                    <code class="text-secondary" style="margin-left: 0.3em;">
                                                        {{ date_format($stats['lastLog']->created_at, 'H:i:s') }}
                                                    </code>
                                                </em>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>


                </div>
            </div>


        </div>
    </div>

    <script src="{{ asset('assets/js/moment.min.js') }}"></script>
    <script src="{{ url('assets/js/vue.min.js') }}"></script>
    <script>
        var timeBlock = new Vue({
            el: '#timeBlock',
            data: {
                currentTime: moment().format('DD.MM.YYYY HH:mm:ss')
            },
            created: function () {
                setInterval(() => { this.currentTime = moment().format('DD.MM.YYYY HH:mm:ss'); }, 1000);
            }
        });

    </script>
@endsection
