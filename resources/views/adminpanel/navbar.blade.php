    <nav class="navbar navbar-light navbar-expand-lg" style="background: url(/images/footer_Background_admin.jpg);">
        <div class="container">
            {{-- <a class="navbar-brand active" href="#">Container</a> --}}

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBarCollapsable" aria-controls="navBarCollapsable" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navBarCollapsable">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item{{ ends_with(url()->current(), url('adminpanel')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel') }}"><i class="fa fa-desktop"></i> Сводка</a></li>
                    <li class="nav-item{{ str_contains(url()->current(), 'emitter') ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/emitters') }}"><i class="fa fa-file-alt"></i> Эмитенты</a></li>
                </ul>


                <!-- <ul class="navbar-nav justify-content-end">
                    <li class="nav-item{{ str_contains(url()->current(), 'setting') ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/settings/show') }}"><i class="fa fa-sliders-h"></i> Настройки</a></li>
                </ul> -->


                @if (!Auth::guest())
                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item"><a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out-alt"></i> Выход</a></li>
                    </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif
            </div>
        </div>
    </nav>
