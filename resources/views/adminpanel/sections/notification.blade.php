@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h5 class="text-danger">Уведомление</h5>
                </div>

                <div class="card-body">
                    @if (isset($updating))
                        @if ($updating)
                            <div class="lead alert alert-success text-center">
                                <i class="fa fa-thumbs-up"></i> Изменения настроек успешно сохранены
                            </div>
                        @else
                            <div class="lead alert alert-warning text-center">
                                <i class="fa fa-exclamation-triangle"></i> Настройки остались без изменений
                            </div>
                        @endif
                    @endif
                </div>
            </div>

        </div>
    </div>






@endsection
