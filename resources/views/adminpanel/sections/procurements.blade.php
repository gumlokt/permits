@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5 class="text-danger">{{ $title }}</h5>
                        </div>

                        @if ('Архив закупок' == $title)
                            <div class="col">
                                <div class="text-right">
                                    <h5>
                                        <a href="{{ url('adminpanel/archives/destroyall') }}" class="text-danger"><i class="fa fa-times"></i> Удалить все</a>
                                    </h5>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">

                    @if (isset($procurements) and !empty($procurements[0]))
                        @include('adminpanel.includes.table')
                    @else
                        <div class="text-center">Нет данных для отображения</div>
                    @endif

                </div>
            </div>

        </div>
    </div>







@endsection
