@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <h5 class="card-header text-danger">
                    Настройки
                </h5>

                <div class="card-body">

                    <p>
                        <em class="text-muted">Раздел находится в процессе разработки...</em>
                    </p>
                    <br>


                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#cars" role="tab">Автомобили</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#permits" role="tab">Пропуска</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#users" role="tab">Пользователи</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#emitters" role="tab">Эмитенты</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="cars" role="tabpanel"><em class="text-muted"><br>Автомобили...</em></div>
                        <div class="tab-pane" id="permits" role="tabpanel"><em class="text-muted"><br>Пропуска...</em></div>
                        <div class="tab-pane" id="users" role="tabpanel"><em class="text-muted"><br>Пользователи...</em></div>
                        <div class="tab-pane" id="emitters" role="tabpanel"><em class="text-muted"><br>Эмитенты...</em></div>
                    </div>

                </div>
            </div>

        </div>
    </div>







@endsection
