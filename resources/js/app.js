
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/App.vue'));


require('./icons/icons');


import VueRouter from 'vue-router';
import App from './components/App.vue';


import Toasted from 'vue-toasted';
Vue.use(Toasted, {
    theme: "outline",
    position: 'top-center',
    className: 'myToast',
    containerClass: 'myToastContainer',
    duration: 1000,
    action : {
        text : 'Закрыть',
        onClick : (e, toastObject) => {
            toastObject.goAway(0);
        }
    }

});


import Store from './store/store';
import { routes } from './routes/routes';
// import { faVolumeDown } from '@fortawesome/free-solid-svg-icons';

Vue.config.productionTip = false;



Vue.use(VueRouter);

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});


export const bus = new Vue();

const app = new Vue({
    el: '#app',
    store: Store,
    router: router,
    render: h => h(App)
});

