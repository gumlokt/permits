import moment from "moment";
export default {
    state: {
        permitActionPath: "/permit/store",
        permitHeader: "Добавить новый пропуск",

        permits: [],

        categories: [],
        transfers: [],
        emitters: [],

        permitFilteringPath: "/permits/getvalid",
        totalRows: 0,
        rowsRoDisplay: 30, // if this variable equals 0 rows (or false), then it is used for exporting logs to Excel and means "fetch all appropriate rows"
        permitsPage: 1,
        pages: 1,

        permitData: {
            id: "",
            number: "",
            model: "",
            plate: "",
            owner: "",
            customer: "",
            weapon: false,
            privileged: false,
            start: "",
            end: "",
            person: "",
            emitter: "",
            abbr: "",
            kpp1: true,
            kpp2: true
        },

        permitSuggestions: {
            number: [],
            model: [],
            plate: [],
            owner: [],
            customer: [],
            person: [],
        },
    },

    getters: {
        permits: function (state) {
            return state.permits;
        },

        allPermitIds: function (state) {
            let ids = [];

            state.permits.forEach((item) => {
                if (!item.transferred) {
                    ids.push(item.id);
                }
            });

            return ids;
        },
    },

    mutations: {
        populatePermits: function (state, payload) {
            axios
                .post(state.permitFilteringPath, {
                    filters: {
                        categories: state.categories,
                        transfers: state.transfers,
                        emitters: state.emitters,

                        model: state.permitData.model,
                        plate: state.permitData.plate,
                        owner: state.permitData.owner,

                        number: state.permitData.number,
                        start: state.permitData.start,
                        end: state.permitData.end,
                        transferred: state.permitData.transferred,

                        customer: state.permitData.customer,
                        weapon: state.permitData.weapon,
                        kpp1: state.permitData.kpp1,
                        kpp2: state.permitData.kpp2,
                        privileged: state.permitData.privileged,
                        person: state.permitData.person,
                    },
                    page: state.permitsPage,
                    rows: state.rowsRoDisplay,
                })
                .then((response) => {
                    state.totalRows = +response.data.totalRows; // + converts data from string to number

                    if (state.totalRows > state.rowsRoDisplay) {
                        state.pages = Math.floor(
                            state.totalRows / state.rowsRoDisplay
                        );

                        if (state.totalRows % state.rowsRoDisplay) {
                            state.pages++;
                        }
                    } else {
                        state.pages = 1;
                    }

                    state.permits = response.data.permits;
                });
        },

        updatePermitData: function (state, payload) {
            // [:print:] matches a visible character [\x21-\x7E]
            // \s+ matches any whitespace character (equal to [\r\n\t\f\v ])
            var pattern =
                /[\d\w\s\+\-\*\/~!@"#№$;%\^:&?<>\(\)\[\]\{\}а-яА-ЯёЁ]+/;

            // if ("weapon" == payload.field) {
            //     console.log(
            //         "field: " + payload.field + "; value: " + payload.value
            //     );
            // }

            if ("privileged" == payload.field) {
                console.log(
                    "field: " + payload.field + "; value: " + payload.value
                );
            }

            if (pattern.test(payload.value)) {
                if (!/^\s+/.test(payload.value)) {
                    state.permitData[payload.field] = payload.value;
                }
                // console.log('field: ' + payload.field + '; value: ' + payload.value);
            } else {
                state.permitData[payload.field] = "";
            }
        },

        setPermitsPage: function (state, payload) {
            state.permitsPage = payload.page;
        },

        setPermitFilteringPath: function (state, payload) {
            state.permitFilteringPath = payload;
        },

        setCategories: function (state, payload) {
            state.categories = payload;
        },

        setTransfers: function (state, payload) {
            state.transfers = payload;
        },

        setPermitEmitters: function (state, payload) {
            state.emitters = payload;
        },

        updatePermitSuggestions: function (state, payload) {
            state.permitSuggestions[payload.field] = payload.value;
        },

        clearPermitSuggestions: function (state) {
            state.permitSuggestions = {
                number: [],
                customer: [],
                person: [],
                owner: [],
                model: [],
                plate: [],
            };
        },

        clearPermitData: function (state) {
            state.permitData = {
                id: "",
                number: "",
                customer: "",
                weapon: false,
                privileged: false,
                person: "",
                owner: "",
                model: "",
                plate: "",
                start: "",
                end: "",
                emitter: "",
                abbr: "",
                kpp1: false,
                kpp2: false,
            };
            state.permitActionPath = "/permit/store";
            state.permitHeader = "Добавить новый пропуск";
        },

        addPermit: function (state, payload) {
            state.permitActionPath = "/permit/store";
            state.permitHeader = "Добавить новый пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
                // state.permitData.number = +result.number + 1;
                axios.get("/permit/number").then((response) => {
                    state.permitData.number = +response.data + 1;
                });
                state.permitData.start = "";
                state.permitData.end = "";
            });
        },

        editPermit: function (state, payload) {
            state.permitActionPath = "/permit/update";
            state.permitHeader = "Отредактировать пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.weapon
                    ? (result.weapon = true)
                    : (result.weapon = false);

                result.privileged
                    ? (result.privileged = true)
                    : (result.privileged = false);

                result.kpp1
                    ? (result.kpp1 = true)
                    : (result.kpp1 = false);

                result.kpp2
                    ? (result.kpp2 = true)
                    : (result.kpp2 = false);

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
            });
        },

        invalidatePermit: function (state, payload) {
            state.permitActionPath = "/permit/" + payload + "/invalidate";
            state.permitHeader = "Аннулировать пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
            });
        },

        setLastPermitNumber: function (state, payload) {
            axios.get("/permit/number").then((response) => {
                state.permitData.number = +response.data + 1;
            });
        },

        setStartAndEndDate: function (state, payload) {
            axios
                .get("/permit/last")
                .then((response) => {
                    let result = response.data;

                    state.permitData.start = moment(
                        new Date(result.start)
                    ).format("DD.MM.YYYY");
                    state.permitData.end = moment(new Date(result.end)).format(
                        "DD.MM.YYYY"
                    );
                })
                .catch((error) => {
                    console.log(error.response.data);
                });
        },
    },

    actions: {},
};
