export default {
    state: {
        actualEmitters: [],

        direction: [],
        emitters: [],

        logActionPath: '/logs/last',
        plate: '',
        lastlogs: [],
        totalRows: 0,
        rowsRoDisplay: 30, // if this variable equals 0 rows (or false), then it is used for exporting logs to Excel and means "fetch all appropriate rows"
        page: 1,
        pages: 1,

        permitIsSelected: false,
        permitIsValid: false,

        logData: { 
            customer: '', 
            driver: '', 
            destination: '', 
            comment: '', 
            cargo: ''
        },

        logSuggestions: { 
            customer: [], 
            driver: [], 
            destination: [], 
            comment: [], 
            cargo: []
        },

        statusMessages: {
            selectPermit:           'Выберите пропуск по гос. номеру авто...',
            carNotRegistered:       'Авто с таким гос. номером нет...',
            permitExpired:          'Срок действия этого пропуска истёк...',
            validFrom:              'Действие пропуска начнётся с ',
            completeOtherFields:    'Введите данные о водителе, грузе и пр...',
            registerEvent:          'Зарегистрируйте Въезд либо Выезд...'
        }
    },

    getters: {
        actualEmitters: function (state) {
            return state.actualEmitters;
        },

        lastLogs: function (state) {
            return state.lastlogs;
        },

        totalRows: function (state) {
            return state.totalRows;
        },

        rows: function (state) {
            return state.rowsRoDisplay;
        },

        getLogData: function (state) {
            return state.logData;
        },

        getStatusMessages: function (state) {
            return state.statusMessages;
        },

        requiredFieldsAreCompleted: function (state) {
            if ('' != state.logData.customer & '' != state.logData.driver & '' != state.logData.destination & '' != state.logData.cargo) {
                return true;
            } else {
                return false;
            }    
        }
    },

    mutations: {
        populateLastLogs: function (state, payload) {
            axios.post(state.logActionPath, {
                plate: state.plate,
                page: state.page,
                rows: state.rowsRoDisplay
            }).then(response => {
                state.totalRows = +response.data.totalRows; // + converts data from string to number

                if (state.totalRows > state.rowsRoDisplay) {
                    state.pages = Math.floor(state.totalRows/state.rowsRoDisplay);

                    if (state.totalRows % state.rowsRoDisplay) {
                        state.pages++;
                    }
                } else {
                    state.pages = 1;
                }

                state.lastlogs = response.data.logs;
            });
        },

        updateLastLogs: function (state, payload) {
            state.lastlogs = payload;
        },

        setLogActionPath: function (state, payload) {
            state.logActionPath = payload;
        },

        setPlate: function (state, plate) {
            state.plate = plate;
        },

        setPage: function (state, payload) {
            state.page = payload.page;
        },

        setDirection: function (state, payload) {
            state.direction = payload;
        },

        setEmitters: function (state, payload) {
            state.emitters = payload;
        },

        reverseDirection: function (state, payload) {
            axios.get('/log/' + payload.id + '/reverse').then(response => {
                if (response.data.error) {
                    console.log(response.data.message);
                } else {
                    if ('in' == state.lastlogs[payload.index].direction) {
                        state.lastlogs[payload.index].direction = 'out';
                    } else {
                        state.lastlogs[payload.index].direction = 'in';
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        },

        updateLogData: function (state, payload) {
            // [:print:] matches a visible character [\x21-\x7E]
            // \s+ matches any whitespace character (equal to [\r\n\t\f\v ])
            var pattern = /[\d\w\s\+\-\*\/~!@"#№$;%\^:&?<>\(\)\[\]\{\}а-яА-ЯёЁ]+/;

            if (pattern.test(payload.value)) {
                if (!/^\s+/.test(payload.value)) {
                    state.logData[payload.field] = payload.value;
                }
                // console.log('field: ' + payload.field + '; value: ' + payload.value);
            } else {
                state.logData[payload.field] = '';
            }
        },

        updateLogSuggestions: function (state, payload) {
            state.logSuggestions[payload.field] = payload.value;
        },

        clearLogSuggestions: function (state) {
            state.logSuggestions = { customer: [], driver: [], destination: [], comment: [], cargo: [] };
        },

        clearAllLogData: function (state) {
            state.permitIsSelected = false;
            state.permitIsValid = false;
            state.logData = { customer: '', driver: '', destination: '', comment: '', cargo: '' };
            state.logSuggestions = { customer: [], driver: [], destination: [], comment: [], cargo: [] };
        },
    },

    actions: {
    }
}
