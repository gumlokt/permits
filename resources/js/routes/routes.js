import Permits from '../components/Permits/Permits';
import Settings from '../components/Settings/Settings';


export const routes = [
    { path: '/', component: Permits },
    // { path: '/permits', component: Permits },
    // { path: '/search', component: Search },
    // { path: '/login', component: Login },
    { path: '/settings', component: Settings },
    // { path: '*', redirect: to => { return '/' }}
];
